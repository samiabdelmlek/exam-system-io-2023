#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{

    FILE *inputFile;
    int i;

    int index;
    int c;

    char *buffer = 0;
    long length;
    int compteur = -1;

    opterr = 0;

    while ((c = getopt(argc, argv, "picrs:")) != -1)
        switch (c)
        {
        case 'p':
            if (argc == 1)
            {
                perror("Aucun fichier mit en argument");
            }
            else
            {
                inputFile = fopen(argv[2], "r");
                if ((i = fgetc(inputFile)) == EOF)
                {
                    printf("fichier vide\n");
                    return 1;
                }

                while ((i = fgetc(inputFile)) != EOF)
                {
                    fputc(i, stdout);
                }

                fclose(inputFile);
            }
            break;
        case 'i':
            if (argc == 1)
            {
                perror("Aucun fichier mit en argument");
            }
            else
            {
                inputFile = fopen(argv[2], "r");
                FILE *sortie = fopen("../samples/output.txt", "w");
                if ((i = fgetc(inputFile)) == EOF)
                {
                    printf("fichier vide\n");
                    return 1;
                }

                while ((i = fgetc(inputFile)) != EOF)
                {
                    fprintf(sortie, "%c", i);
                }

                fclose(inputFile);
                fclose(sortie);
            }
            break;
        case 'c':
            if (argc == 1)
            {
                perror("Aucun fichier mit en argument");
            }
            else
            {
                inputFile = fopen(argv[2], "r");
                FILE *sortie = fopen("../samples/output.txt", "w");
                if ((i = fgetc(inputFile)) == EOF)
                {
                    printf("fichier vide\n");
                    return 1;
                }

                const char *separators = " ,.-!";

                if (inputFile)
                {
                    fseek(inputFile, 0, SEEK_END);
                    length = ftell(inputFile);
                    fseek(inputFile, 0, SEEK_SET);
                    buffer = malloc(length);
                    if (buffer)
                    {
                        fread(buffer, 1, length, inputFile);
                    }
                    fclose(inputFile);
                }
                if (buffer)
                {
                    char *strToken = strtok(buffer, separators);
                    while (strToken != NULL)
                    {
                        // printf("%s\n", strToken);
                        strToken = strtok(NULL, separators);
                        compteur++;
                    }
                }
                printf("Nombre de mots : %d\n", compteur);
                return 0;
            }
            break;
        case 'r':
            if (argc == 1)
            {
                perror("Aucun fichier mit en argument");
            }
            else
            {
                inputFile = fopen(argv[2], "r");
                FILE *sortie = fopen("../samples/output.txt", "w");
                if ((i = fgetc(inputFile)) == EOF)
                {
                    printf("fichier vide\n");
                    return 1;
                }

                const char *separators = " ,.-!";

                if (inputFile)
                {
                    fseek(inputFile, 0, SEEK_END);
                    length = ftell(inputFile);
                    fseek(inputFile, 0, SEEK_SET);
                    buffer = malloc(length);
                    if (buffer)
                    {
                        fread(buffer, 1, length, inputFile);
                    }
                    fclose(inputFile);
                }
                if (buffer)
                {
                    char *tab[282];
                    char *strToken = strtok(buffer, separators);
                    while (strToken != NULL)
                    {
                        // printf("%s\n", strToken);
                        strToken = strtok(NULL, separators);
                        compteur++;
                        tab[compteur] = strToken;
                        printf("%s", tab[compteur]);
                    }
                    for (i = 0; i < compteur; i++){
                        if (tab[i] == argv[3])
                        {
                            tab[i] = argv[4];
                        }
                        
                    }
                    printf("%s", tab);
                    FILE *replace = fopen("../samples/output_ex4.txt", "w");
                    for (i = 0; i < sizeof(tab); i++)
                    {
                        fprintf(replace, "%c", tab[i]);
                    }
                    fclose(replace);
                }

                return 0;
            }
            break;
        case 's':

            break;
        case '?':
            if (optopt == 'c')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint(optopt))
                fprintf(stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf(stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
            return 1;
        default:
            abort();
        }

    for (index = optind; index < argc; index++)
        printf("Non-option argument %s\n", argv[index]);
    return 0;
}