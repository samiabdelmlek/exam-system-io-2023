# Programmation système: les Entrées/Sorties

## Objectif :

* Créer un programme en C qui gère les entrées / sorties de fichiers.
* Apprendre à utiliser les fonctions standard C pour lire et écrire des fichiers.
* Votre programme devra être utiliser `getopt()` pour permettre à l'utilisateur de choisir les fonctions à utiliser [Documentation et example getopt()](http://manpagesfr.free.fr/man/man3/getopt.3.html)

## Tâches :

1. Écrire un programme en C qui lit un fichier texte (par exemple, "samples/input.txt") et l'affiche à l'écran. Utilisez la fonction `fopen()` pour ouvrir le fichier et `fgets()` pour lire les lignes.

        ./exam -p input.txt

2. Modifiez le programme pour écrire les lignes lues dans un nouveau fichier (par exemple, "samples/output.txt"). Utilisez la fonction `fopen()` pour ouvrir le fichier et `fputs()` pour écrire les lignes.
    
        ./exam -i input.txt

3. Ajoutez une fonction qui compte le nombre de mots dans le fichier d'entrée et l'affiche à l'écran. Utilisez la fonction `strtok()` pour séparer les mots.

        ./exam -c input.txt

4. Ajoutez une fonction qui remplace toutes les occurrences d'un mot donné (par exemple, "Max") par un autre mot (par exemple, "Linus") dans le fichier d'entrée. Utilisez la fonction `strstr()` pour rechercher des occurrences.

        ./exam -r input.txt Max Linus

5. Ajoutez une fonction qui trie les lignes du fichier d'entrée en ordre alphabétique et les écrit dans le fichier de sortie. Utilisez la fonction `qsort()` pour trier les lignes.

        ./exam -s input.txt


    >Note : N'oubliez pas de fermer les fichiers avec la fonction fclose() lorsque vous avez fini de les utiliser.

## Organisation
1. créez un compte Gitlab
2. Forkez le projet sur un compte de l'équipe
3. Ajoutez les collaborateurs dans les settings
4. Clonez le projet chacun en local (`git clone URL_PROJET`)
5. rendez votre travail en effectuant une Merge Request vers le projet principal.


* Vous rendrez votre travail dans une merge request Git du projet qui vous sera mis à disposition.

* Vous mettrez en place un Makefile pour compiler le projet, les binaires devront être stockés dans un répertoire "bin/" du dossier courant et ignorez par Git.


Bonne chance!


NB : argv[1] est forcement un parametre(exemple : -r)